#!/bin/sh

# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Manuel Raaf in 2015 ff. © http://badw.de

PARAM="$1"
if [ "$PARAM" = "" ] 
then 
	PARAM="daily" 
fi

USER=""
PASS=""
DATABASES=("shurl_purl" "shurl_tiny")
EXCLUDE_TABLES_CONTAINING=("backup")
FOLDER="ABSOLUTE_PATH_TO_YOUR_APPLICATION/backups/$PARAM"
DATE=`date +%Y%m%d%H%M`

# loops through the array containing all databases that should be backuped
for DB in ${DATABASES[*]}
do
	if [ "$DB" != "" ]
	then
		# if there are tables to skip:
		if [ "$EXCLUDE_TABLES_CONTAINING" != "" ] &> /dev/null
		then			
			EXCLUDE=""
			for TBL in ${EXCLUDE_TABLES_CONTAINING[*]}
			do
				# creates the code for excluding multiple tables 
				EXCLUDE="$EXCLUDE AND TABLE_NAME NOT REGEXP '$TBL'"
			done			

			rm $FOLDER/${DATE}_${DB}.sql &> /dev/null	
			for TBL in `mysql -v -u $USER -p$PASS $DB -N -e "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE (TABLE_SCHEMA='$DB'$EXCLUDE)"` 
			do 
				# appends the sql-dump to output file 				
				echo -e "\toptimizing and creating dump of ${DB}.${TBL}"
				mysql -v -D $DB -u $USER -p$PASS -e "REPAIR TABLE ${TBL}"
				mysqldump -v -u $USER -p$PASS --lock-tables=false $DB $TBL >> $FOLDER/${DATE}_${DB}.sql				
			done
		# simply dump the whole database
		else	
			echo -e "\tcreating dump of complete $DB"
			mysqldump -u $USER -p$PASS --lock-tables=false $DB > $FOLDER/${DATE}_${DB}.sql			
		fi
		
		# standard compression using gzip 
		echo -e "\tcreating compressed archive"
		basename "${FOLDER}/${DATE}_${DB}.sql" | xargs tar -zcf $FOLDER/${DATE}_${DB}.tar.gz -C $FOLDER &> /dev/null	
		
		echo -e "\tdeleting sql dump"
		rm $FOLDER/${DATE}_${DB}.sql &> /dev/null			
	fi
done
echo -e "\tdone! :)"
