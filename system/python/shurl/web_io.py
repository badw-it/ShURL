# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Manuel Raaf in 2016 ff., framework by Stefan Müller in 2013 ff.
# © http://badw.de
import os

from beaker.middleware import SessionMiddleware

import __init__
import bottle
import re
import base64
import traceback
from bottle import HTTPError, redirect, request, response, static_file, template
from shurl import auth
import urllib.parse

class App(bottle.Bottle):
    '''
    A bottle application object.
    See the docstring of :func:`bottleapp.get` in :path:`../../bottleapp.py`.
    '''
    def __init__(
            self,
            dao_class: type,
            dao_kwargs: 'Dict[str, Any]',
            https: bool,
            host: str,
            port: str,
            debug: bool,
            server: str,
            views_paths: str,
            cssjs_path: str,
            ):
        super().__init__()
        self.https = https
        self.host = host
        self.port = port
        self.external_port = None
        self.debug = debug
        self.server = server
        self.cssjs_path = os.path.abspath(cssjs_path) + os.sep
        self.views_paths = [ os.path.abspath(p) + os.sep for p in views_paths ]
        bottle.TEMPLATE_PATH = self.views_paths
        self.dao = dao_class(**dao_kwargs)
        
        self.set_routes(self.dao)
        self.template_ids = {
                os.path.splitext(os.path.basename(path))[0]
                for dirpath in self.views_paths
                for path in os.listdir(dirpath)
                }        

    def run(self) -> None:
        '''
        Run :param:`self` by calling :meth:`bottle.run`. Before or in this call,
        you may apply middleware, e.g. instead of mere ``app = self``, pass
        ``app = SessionMiddleware(self, self.dao.session_options)``.
        '''
        bottle.run(
                app = SessionMiddleware(self, self.dao.session_options),
                host = self.host,
                port = self.port,
                debug = self.debug,
                server = self.server,
                certfile = self.dao.ssl_certfile if self.https else '',
                keyfile = self.dao.ssl_keyfile if self.https else '',
                )

    def set_routes(self, dao) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.hook('before_request')
        def check():
            path = request.urlparts[2]           
            if not path.startswith('/-/') and not path.startswith('/_/') and \
               not re.match(r'/?[^/]{4,}$', path):
                if '/admin' in path and not '/admin/login' in path:
                    if not auth.admin_login(
                            request.forms.user,
                            request.forms.password,
                            request.environ.get('beaker.session'),
                            dao.connect,
                            dao.db_for_credentials,
                            ):
                        lang_id = path.split('/')[0]
                        lang_id = lang_id if lang_id in dao.lang_ids else dao.default_lang_id                    
                        redirect('/{}/admin/login'.format(lang_id))
                elif dao.use_login and not get_purl_status(request.environ, dao.purl_domain) \
                     and not any([x in path for x in ('/api', '/login', '/logout', '/about', '/contact',)]):
                    if not auth.login(
                            request.forms.user,
                            request.forms.password,
                            request.environ.get('beaker.session'),
                            dao.ldap_connection_info if dao.use_ldap else None,
                            dao.connect,
                            dao.db_for_credentials,
                            ):                   
                        lang_id = path.split('/')[0]
                        lang_id = lang_id if lang_id in dao.lang_ids else dao.default_lang_id
                        redirect('/{}/login'.format(lang_id))
            
        @self.error(404)
        def error(error):
            return template(
                    'not_found',
                    request = request,
                    dao = dao,
                    kwargs = {
                        'lang_id': dao.default_lang_id,
                        'page_id': 'error',
                        'error': str(error),
                        }
                    )

        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which specify no page nor language.
            Assume the default page and the default language of the site.

            The redirected request will be dealt with by :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/{}/{}'.format(dao.default_lang_id, dao.default_page_id))

####################################################################################################
########################### A P I   A R E A ########################################################
####################################################################################################
        @self.route('/api')
        def api():
            host = request.query.get('a') or None
            if (dao.api_active == True):
                referer = request.environ.get('HTTP_REFERER') or request.get_header('referer') \
                          or None
                #print(1, referer)
                if referer:
                    try:
                        referer = urllib.parse.urlparse(referer).netloc
                    except Exception as e:
                        referer = request.environ.get('HTTP_REFERER') or request.get_header('referer') \
                                  or None
                #print(2, referer)
                if referer:
                    host = base64.b64decode(urllib.parse.unquote(host)).decode('utf-8')
                    url = host
                    pattern = '^(?:(?:http|ftp)s?:\/\/)?{}'
                    referer_allowed = False
                    for a in dao.api_referers:
                        if re.match(re.compile(pattern.format(re.escape(a))), referer):
                            referer_allowed = True
                            break
                    #print(3, host, referer_allowed)
                    if referer_allowed:
                        is_purl_domain = get_purl_status(request.environ, dao.purl_domain)
                        url_id, status = dao.get_url_id(url, is_purl_domain, None, None, None)
                        return '{}://{}/{}'.format(request.urlparts[0], request.urlparts[1], url_id) \
                               if url_id else None
            return None

####################################################################################################
####################################################################################################
####################################################################################################

        @self.route('/-/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.
            '''
            return static_file(path, self.cssjs_path)

        @self.route('/<url_id>')
        def react_on_url_id(url_id):
            is_purl_domain = get_purl_status(request.environ, dao.purl_domain)
            #is_purl_domain = True
            referer = request.environ.get('HTTP_REFERER') \
                    or request.get_header('referer') or ''
            url = dao.get_url(url_id, is_purl_domain, referer)
            if url:
                parse_result = urllib.parse.urlparse(url)
                url = parse_result.scheme + '://' + parse_result.netloc + '/'
                if parse_result.path:
                    url += parse_result.path.lstrip('/')
                if parse_result.query:
                    url += '?' + urllib.parse.quote_plus(parse_result.query, safe='/&?=%#', encoding="utf-8")
                if parse_result.fragment:
                    url += '#' + parse_result.fragment
                # redirect(url)
            return template(
               'react_on_url_id',
                request = request,
                dao = dao,
                kwargs = {
                    'do_redirect': 1,
                    'lang_id': dao.default_lang_id,
                    'page_id': url_id,
                    'is_purl_domain': is_purl_domain,
                    'url_id': url_id,
                    'url': url,
                    }
                )

####################################################################################################
###################### A D M I N    A R E A ########################################################
####################################################################################################

        @self.route('/<lang_id>/admin')
        def redirect_admin(lang_id):
            redirect('/{}/admin/start'.format(lang_id))

        @self.route('/<lang_id>/admin/logout')
        def admin_logout(lang_id):
            auth.admin_logout(request.environ.get('beaker.session'))
            headers = {
                    'WWW-Authenticate': 'Basic realm="Login"',
                    'Authorization': 'Basic realm="Login"',
                    }
            body = template(
                    'admin_login',
                    request = request,
                    dao = dao,
                    kwargs = {
                        'lang_id': lang_id or dao.default_lang_id,
                        'page_id': 'admin_logout',
                        'message': 'Sie sind ausgeloggt.',
                        }
                    )
            raise bottle.HTTPResponse(status = 401, body = body, header = headers)

        @self.route('/<lang_id>/admin/login', method = 'POST')
        def admin_login(lang_id):             
            if auth.admin_login(
                    request.forms.user,
                    request.forms.password,
                    request.environ.get('beaker.session'),
                    dao.connect,
                    dao.db_for_credentials,
                    ):
                redirect('/{}/admin/start'.format(lang_id))
            else:
                return template(
                    'admin_login',
                    request = request,
                    dao = dao,
                    kwargs = {
                        'lang_id': lang_id or dao.default_lang_id,
                        'page_id': 'admin_login',
                        'message': 'Passwort und/oder Nutzername sind nicht korrekt!',
                        }
                    )
            
        @self.route('/<lang_id>/admin/accept')
        def admin_accept(lang_id):
            status = dao.accept(request.query['url_id'] or '')
            status['action'] = 'accept'
            return template('admin_start', request = request, dao = dao,
                    kwargs = {
                        'lang_id': lang_id,
                        'page_id': 'accept',
                        'status': status,
                        }
                    )

        @self.route('/<lang_id>/admin/refuse')
        def admin_refuse(lang_id):
            status = dao.refuse(request.query['url_id'] or '')
            status['action'] = 'refuse'
            return template('admin_start', request = request, dao = dao,
                    kwargs = {
                        'lang_id': lang_id,
                        'page_id': 'refuse',
                        'status': status,
                        }
                    )

        @self.route('/<lang_id>/admin/delete_refused')
        def admin_delete_refuse(lang_id):
            status = dao.delete_refused()
            status['action'] = 'delete_refused'
            return template('admin_start', request = request, dao = dao,
                    kwargs = {
                        'lang_id': lang_id,
                        'page_id': 'delete_refused',
                        'status': status,
                        }
                    )

        @self.route('/<lang_id>/admin/create_user', method = 'POST')
        def admin_create_user(lang_id):
            name = request.forms.userName
            password = request.forms.userPassword
            valid_name = True
            valid_password = True
            name_exists = False
            error = ''
            if not name.strip():
                valid_name = False
            elif not password.strip():
                valid_password = False
            else:
                try:
                    name_exists = dao.create_user(name, password)
                except Exception as e:
                    print (traceback.format_exc())
                    error = str(e)
            return template(
                    'admin_create_user',
                    request = request,
                    dao = dao,
                    kwargs = {
                        'lang_id': lang_id,
                        'page_id': 'create_user',
                        'valid_name': valid_name,
                        'valid_password': valid_password,
                        'name_exists': name_exists,
                        'error': error,
                        }
                    )

        @self.route('/<lang_id>/admin/change_password', method = 'POST')
        def admin_change_password(lang_id):
            password = request.forms.changePassword1
            password_repeated = request.forms.changePassword2
            user = request.environ.get('beaker.session').get('admin')            
            matching = True
            valid = True
            error = ''
            if not password.strip():
                valid = False
            elif password != password_repeated:
                matching = False
            elif user:
                try:
                    dao.change_password(password, user)
                except Exception as e:
                    error = str(e)
            return template(
                    'admin_change_password',
                    request = request,
                    dao = dao,
                    kwargs = {
                        'lang_id': lang_id,
                        'page_id': 'change_password',
                        'matching': matching,
                        'valid': valid,
                        'error': error,
                        }
                    )

        @self.route('/<lang_id>/admin/<page_id>')
        def admin_page(lang_id, page_id):
            template_id = 'admin_' + page_id
            if template_id in self.template_ids:
                return template(
                        template_id,
                        request = request,
                        dao = dao,
                        kwargs = {
                            'lang_id': lang_id,
                            'page_id': page_id,
                            }
                        )
            else: raise HTTPError(404, 'The page has not been found.')

####################################################################################################
####################################################################################################
####################################################################################################

        @self.route('/<lang_id>/login', method =  'POST')
        def login(lang_id):
            if get_purl_status(request.environ, dao.purl_domain) or auth.login(
                    request.forms.user,
                    request.forms.password,
                    request.environ.get('beaker.session'),
                    dao.connect,
                    dao.db_for_credentials,
                    dao.ldap_connection_info if dao.use_ldap else None                    
                    ):
                redirect('/{}/start'.format(lang_id))
            else:
                return template(
                    'login',
                    request = request,
                    dao = dao,
                    kwargs = {
                        'lang_id': lang_id or dao.default_lang_id,
                        'page_id': 'login',
                        'message': 'Passwort und/oder Nutzername sind nicht korrekt!',
                        }
                    )   

        @self.route('/<lang_id>/logout')
        def logout(lang_id):
            auth.logout(request.environ.get('beaker.session'))
            headers = {
                    'WWW-Authenticate': 'Basic realm="Login"',
                    'Authorization': 'Basic realm="Login"',
                    }
            body = template(
                    'login',
                    request = request,
                    dao = dao,
                    kwargs = {
                        'lang_id': lang_id or dao.default_lang_id,
                        'page_id': 'login',
                        'message': 'Sie sind ausgeloggt.',
                        }
                    )
            raise bottle.HTTPResponse(status = 401, body = body, header = headers)
            
        @self.route('/<lang_id>/start', method='POST')
        def start(lang_id):
            purl_mail = request.forms.purl_mail.strip()
            url = request.forms.url.strip()
            date = request.forms.date.strip()
            purl_checkbox = int(request.forms.purl_checkbox.strip() or False)
#             print(request.forms.stats_checkbox)
            stats_checkbox = int(request.forms.stats_checkbox.strip() or False)
            url_id, status = dao.get_url_id(url, purl_checkbox, date, purl_mail, get_session_user(request.environ) if stats_checkbox == 1 else None)
            if dao.log_accesses:
                dao.log(purl_checkbox, url_id, url, 'w', request.environ.get('HTTP_REFERER'))
            if purl_checkbox and url_id and not status['is_activated']:
                dao.send_mail(purl_mail, None, url,
                        'http://{}/{}/admin/start'.format(
                            dao.purl_domain, lang_id))
            if url_id:
                short_url = '{}://{}/{}'.format(
                    request.urlparts[0],
                    request.urlparts[1].replace(dao.subdomain_for_tiny, dao.subdomain_for_purls, 1) if purl_checkbox and status['auto_allow'] is True else request.urlparts[1],
                    url_id
                )
            else:
                short_url = ''
            date = status['date']
            status['datestring'] = date.strftime("%d.%m.%Y, %H:%M") if date else ''
            return template(
                    'start',
                    request = request,
                    dao = dao,
                    kwargs = {
                        'lang_id': lang_id,
                        'page_id': 'start',
                        'url': url,
                        'short_url': short_url,
                        'purl_checkbox': purl_checkbox,
                        'stats_checkbox': stats_checkbox,
                        'is_purl_domain' : get_purl_status(request.environ, dao.purl_domain),
                        'status': status,
                        }
                    )

        @self.route('/<lang_id>/<page_id>')
        def return_page(lang_id, page_id):
            if page_id in self.template_ids and not page_id.startswith('admin'):
                is_purl_domain = get_purl_status(request.environ, dao.purl_domain)
                if is_purl_domain and page_id == "login":                    
                    redirect('/{}/start'.format(lang_id))
                else:
                    template_id = page_id
                    return template(
                            template_id,
                            request = request,
                            dao = dao,
                            kwargs = {
                                'lang_id': lang_id,
                                'page_id': page_id,
                                'is_purl_domain': is_purl_domain,
                                'ldap_user': get_session_user(request.environ) if page_id == "my_area" else None
                                }
                            )
            else: raise HTTPError(404, 'The page has not been found.')
        
        @self.route('/<_>/<path:path>')
        def return_static_content(_, path):
            '''
            Return a static content file, e.g. an image file.

            :param _: is included in the route so that in a page with the URL
                `/de/start` a relative URL `icons/shortcut.ico` can be
                used, because then the absolute version of this relative URL is:
                `/de/icons/shortcut.ico`.
            '''
            return static_file(
                    path,
                    dao.path,
                    download = bool(request.query.download))
        
def get_purl_status(environ, purl_domain):
    # only uncomment this for testing on a test server where no real purl-subdomain is available
    #return True
    domain = environ.get("HTTP_HOST", '')
    subdomain = domain.split(".")[0].lower() if '.' in domain else ''
    try:
        return True if subdomain == purl_domain.split(".")[0].lower() else False
    except:
        return False

def get_session_user(environ):
    try:
        return environ.get('beaker.session').get('user')
    except:
        return None
        pass