# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller and Manuel Raaf in 2016 ff. © http://badw.de
'''
Set up and start the system. More information is given in :func:`bottleapp.get`.
If no port is given via first argument of the call of this script, 443 is taken.
The second argument defines wheater to start in debug mode or not.
See function "val2bool" inside __init__.py for values that are evaluated to false; others will be true
'''
from gevent import monkey; monkey.patch_all()
import __init__
import bottle
import bottleapp
from shurl import dao_io
from shurl import web_io

bottleapp.run(
    bottleapp.get(
        web_io.App,
        dao_io.DAO,
        dao_kwargs  = {
            'config_path':    __file__ + '/../../../../configuration.utf-8.json',
            'badwords_path':  __file__ + '/../../../../badwords.utf-8.txt',
        },
        host        = '0.0.0.0',
        https       = True,
        # make sure that no other webserver is listening on this port!
        port        = 443 if __init__.port is None else __init__.port,        
        debug       = __init__.debug,
        server      = 'gevent',
        views_paths = [__file__ + '/../views', __file__ + '/../../views'],
    )
)
