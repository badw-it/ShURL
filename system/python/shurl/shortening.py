# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Manuel Raaf in 2016. © http://badw.de
'''
This module contains all functions needed for encoding and decoding shortened
URLs on the base of the given chars and a number as input.
This number usually is a unique ID provided by a DBMS like MySQL.

The functionality comes throug mathematical "bijection"
'''
import re
import urllib.request
import urllib.parse

CHARS = 'Ngzr2cG4CV1bJQowFlE8LnT9Ksxi5tRvd3yhWeZ7IPf6BYUMXupkO0jDqmASHa'
CHARS_REVERSE = dict((c, i) for (i, c) in enumerate(CHARS))
BASE = len(CHARS)
def encode(n):
    s = []
    while True:
        n, r = divmod(n, BASE)
        s.append(CHARS[r])
        if n == 0: break
    return ''.join(reversed(s))

def decode(s):
    n = 0
    for c in s:
        n = n * BASE + CHARS_REVERSE[c]
    return n

def remove_white_spaces(text, keep_single_space = True):
    return re.sub('\s+', ' ' if keep_single_space else '', text.strip())

def is_url_available(url, timeout = 30):
    url = remove_white_spaces(url, False)
    if not re.match(r'(?:https?|ftps?):\/\/', url):
        url = 'http://' + url
    # always change this (mainly "safe") in web_io.py too
    #url = re.sub(r'(https?|ftps?)%3A', '\g<1>:', url)
    #print(url)
    parse_result = urllib.parse.urlparse(url)
    #print(parse_result)
    
    #url = parse_result.scheme +  '://' + parse_result.netloc + '/'
    
    #url = "%s://%s%s%s" % (parse_result.scheme, parse_result.netloc, parse_result.path, "?%s" % urllib.parse.quote_plus(parse_result.query, safe='/&?=%#:', encoding="utf-8") if parse_result.query else "")    
    url = "%s://%s%s%s%s" % (
        parse_result.scheme, 
        parse_result.netloc, 
        parse_result.path, 
        "?%s" % urllib.parse.quote_plus(parse_result.query, safe='/&?=%#:', encoding="utf-8") if parse_result.query else "",
        "#%s" % parse_result.fragment if parse_result.fragment else ""
    )
    #if parse_result.path:
    #    url += parse_result.path.lstrip('/')
    #if parse_result.query:
    #    url += parse_result.query + '?' + urllib.parse.quote_plus(parse_result.query, safe='/&?=%#', encoding="utf-8")
    # print (url)
    try:
        req = urllib.request.Request(
            url,
            data = None,
            headers = {
                # update this from time to time to make sure that modern web-pages do not reject you
                # because your user-agent seems to be too old or too fake
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0'                
                }
            )
        data = urllib.request.urlopen(req, timeout=timeout)
    except Exception as e:
        print(e)
        data = None
    if not data and not re.match('^\d+\.\d+\.\d+\.\d+', url):
        return False
    else:
        return True

def main():
    a = encode(125)
    b = decode(a)
    print(a + " " + str(b))

if __name__ == '__main__':
    main()
