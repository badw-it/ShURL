<main>
	<article class="flush non-source sheet">
		<h3>Passwort ändern</h3>
	  % if request.method == 'POST':
		<div class="card">
			<p class="systemMessage">Systemmitteilung:</p>
		  % if not kwargs.get('valid'):
			<p>Ihr Passwort darf weder leer sein noch ausschließlich aus Leerzeichen bestehen!</p>
		  % elif not kwargs.get('matching'):
			<p>Die Wiederholung stimmt nicht mit der Ersteingabe überein.</p>
		  % elif kwargs.get('error'):
			<p>Es ist ein Fehler aufgetreten:</p>
			<code>{{kwargs['error']}}</code>
		  % else:
			<p>Ihr Passwort wurde geändert.</p>
		  % end
		</div>
	  % end
		<form method="post" onSubmit="return check();">
			Hier können Sie Ihr aktuelles Passwort ändern:<br><br>
			<div class="table">
				<div>
					<div>neues Passwort:</div>
					<div><input type="password" class="normal" name="changePassword1" id="changePassword1" value=""></div>
				</div>
				<div>
					<div>wiederholen:</div>
					<div><input type="password" class="normal" name="changePassword2" id="changePassword2" value=""></div>
				</div>
			</div>
			<input type="submit" name="submit" value="Passwort ändern">
		</form>
	</article>
</main>

<script type="text/javascript">
	function check()	{
		p1 = document.getElementById("changePassword1").value;
		p2 = document.getElementById("changePassword2").value;
		if (p1.replace(/\s+/g,"") === "") {
			alert("Das angegebene Passwort darf nicht leer sein oder nur aus Leerzeichen bestehen!");
			return false;
		} else if (p1 !== p2) {
			alert("Die angegebenen Passwörter stimmen nicht überein!");
			return false;
		} else {
			return true;
		}
	}
</script>\\
% rebase('admin_base.tpl', request = request, dao = dao, kwargs = kwargs)
