<main>
	<article class="flush non-source sheet">
		<h2>Kontakt</h2>
		<p>Fragen und Hinweise werden gerne entgegengenommen unter der Anschrift <i>digitalisierung</i> at <i>badw.de</i>.</p>
		<p>Im Übrigen sei auf das <a href="http://badw.de/data/footer-navigation/impressum.html" target="_blank" title="Impressum der BAdW">Impressum</a> sowie die <a href="http://badw.de/data/footer-navigation/datenschutz.html" target="_blank" title="Datenschutzerklärung der BAdW">Datenschutzerklärung</a> der Bayerischen Akademie der Wissenschaften verwiesen.</p>
		<p>Die Programmierung erfolgte durch Manuel Raaf mit einem Rahmenwerk von Stefan Müller.</p>
	</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)