% message = kwargs.get('message', '')
% user = request.forms.user or ''
% password = request.forms.password or ''
<script type="text/javascript">
	function check() {
		if (document.getElementById("user").value.replace(/\s+/g,"") === "" || document.getElementById("password").value.replace(/\s+/g,"") === "") {
			alert("Bitte geben Sie Ihre Login-Daten vollständig ein.\n\nVielen Dank!");
			return false;
		} else {
			return true;
		}
	}
</script>
<main>
	<article class="flush non-source sheet">
		<h2>Login zum Administrationsbereich des Kurz-URL-Dienstes der BAdW</h2>
		% if message:
			<h2 class="systemMessage">Systemmitteilung:</h2>
			<p>{{!message}}</p>
			<br>
		% end
		<form class="container" method="post" action="/{{kwargs.get('lang_id')}}/admin/login" onSubmit="return check();">
			<div class="table">
				<div>
					<div>Nutzername:</div>
					<div><input type="text" name="user" id="user" value="{{user}}"></div>
				</div>
				<div>
					<div>Passwort:</div>
					<div><input type="password" name="password" id="password" value=""></div>
				</div>
			</div>
			<input type="submit" value="Login">
		</form>
	</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)