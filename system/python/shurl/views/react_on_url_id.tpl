% url = kwargs.get('url', '')
% url_id = kwargs.get('url_id', '')
<main>
	<article class="flush non-source sheet">
	  % if not url:
		<p>
			Es konnte leider kein Ergebnis zur übergebenen Kurz-URL ({{url_id}}) gefunden werden.<br>
			Womöglich hatte die URL ein Ablaufdatum?
		</p>
		<br>
		<p>Falls Sie die URL abgetippt haben, so bedenken Sie bitte, dass die Groß-/Kleinschreibung hierbei wichtig ist!</p>
	  % else:
		Sie werden nun weitergeleitet. Das kann in unter Umständen einen Moment dauern.
		<br><br>
		<p>
			Falls das Ziel eine archivierte Webseite ist (z.B. innerhalb der Domain <em>web.archive.org</em>), so kann die Weiterleitung
			einen Moment dauern oder gar fehlschlagen. Bitte ersuchen Sie es in solch einem Fall erneut.
		</p>
		<br>
		<p>
			Sollte Ihr Browser keine Weiterleitung ausführen, klicken Sie bitte hier, um zur Ziel-URL zu gelangen:
			<br><br>
			<a href="{{url}}" title="Ziel-URL aufrufen">{{url}}</a>
		</p>
	  % end
	</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)