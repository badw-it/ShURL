% url = kwargs.get('url', '')
% short_url = kwargs.get('short_url', '')
% purl_checkbox = kwargs.get('purl_checkbox', False)
% stats_checkbox = kwargs.get('stats_checkbox', False)
% is_purl_domain = kwargs.get('is_purl_domain', False)
% #print(purl_checkbox, is_purl_domain)
% status = kwargs.get('status', {})
% datestring = status.get('datestring', '')
% #print(url, short_url, datestring, status)
<main>
<article class="flush non-source sheet">
	<h2>Kurz-URL-Dienst der BAdW</h2>
	<form method="post" onSubmit="return check();">
		<div class="container">
			<p style="text-align:left;">
				URL, die gekürzt werden soll:
				<input type="text" name="url" id="url" value="{{url}}">
			</p>
		</div>
		% if not is_purl_domain:
			<div class="container">
				<input type="checkbox"{{' checked' if stats_checkbox else ''}} name="stats_checkbox" id="stats_checkbox" value="1"/><label for="stats_checkbox"> Aufrufe zählen?</label><br>
				<p style="text-align:left;">
					<u>Erklärung:</u> Mit der Aktivierung der Funktion stimmen Sie zu, dass Ihre Kennung gespeichert wird,
					um die Aufrufe der Kurz-URL zu zählen und Ihnen eine kleine Statistik dazu im Nutzerbereich anzuzeigen.
				</p>
			</div>
		% end
		<div class="container">
			% if not is_purl_domain:
			  <input type="checkbox"{{' checked' if purl_checkbox else ''}} name="purl_checkbox" id="purl_checkbox" value="1"><label for="purl_checkbox"> persistente URL</label>
			% else :
			  <input type="checkbox" checked style="display:none;" name="purl_checkbox" id="purl_checkbox" value="1">
			% end
			<div class="hidden">
				<p>Ihre E-Mail: <input type="text" class="normal" name="purl_mail" id="purl_mail" value="{{request.forms.purl_mail}}"></p>
				<p><small>Die E-Mail-Adresse ist notwendig, um Ihnen das Ergebnis der Überprüfung Ihrer Anfrage zusenden zu können.</small></p>
			</div>
		</div>
		% if not purl_checkbox or is_purl_domain:
		  <p>gültig bis: <input class="normal" type="text" id="date" name="date" value="{{request.forms.date}}"></p>
		  <p><small>Bitte geben Sie das Datum nach folgendem Muster ein: <i><u>20.09.2016 16:15</u></i></small></p>
		% end
		<input type="submit" id="submitButton" value="Generiere Kurz-URL">
	</form>
  % if status:
	<div class="card">
	% if status['error']:
			<p>Es trat ein Fehler auf. Bitte kontaktieren Sie uns unter <i>{{dao.admin_mail.replace('@', ' at ')}}</i> mit der Angabe Ihrer Eingabe und folgender Meldung:</p>
			<code>{{status['error']}}</code>
	% elif not status['page_available']:
			<p>Die angegebene URL <i>{{url}}</i> konnte nicht aufgerufen werden. Bitte überprüfen Sie Ihre Eingabe auf etwaige Tippfehler. Inhalte, die hinter einem Login liegen, können i.d.R. nicht erreicht werden. Achten Sie bitte auch darauf, dass Umlaute im Domainnamen <u>nicht</u> in Unicode angegeben sind, sondern in ihrer entsprechenden HTML-Codierung (z. B.: <i>%C3%B6</i> anstatt <i>ö</i>). Durch Kopieren der URL aus der Adresszeile des Browsers können Sie dies am besten sicherstellen.<br><br>Bitte haben Sie Verständnis dafür, dass nur erreichbare URLs unterstützt werden. Sofern Sie diese Meldung für einen Irrtum halten, kontaktieren Sie uns bitte mit der Angabe der URL unter <i>{{dao.admin_mail.replace('@', ' at ')}}</i>.<br/>Vielen Dank.</p>
	% else:
		% if purl_checkbox or is_purl_domain:
		  % if url and not status['domain_allowed'] and not status['is_activated']:
			<p>Wir bitten um Verständnis, dass wir für permanente URLs nur bestimmte Domains unterstützen können. Die angegebene URL <i>{{url}}</i> befindet sich nicht innerhalb der erlaubten Domains, welche sind:</p>
			<ul>
			  % for domain in dao.allowed_domains_for_purls:
				<li><a href="{{dao.ensure_url_protocol(domain)}}" target="_blank">{{domain}}</a></li>
			  % end
			</ul>
			<p>Sofern Sie diese Meldung für einen Irrtum halten, kontaktieren Sie uns bitte mit der Angabe der URL unter <i>{{dao.admin_mail.replace('@', ' at ')}}</i>.<br/> Vielen Dank.</p>
		  % end
		  % if any((status['was_rejected'], status['is_activated'], status['is_under_consideration'], status['auto_allow'])):
			% if not status['auto_allow']:
			  <p>Diese URL wurde bereits als PURL beantragt.
			% end
			% if status['was_rejected']:
				Die URL wurde abgelehnt — bitte haben Sie Verständnis dafür, dass sie daher nicht erneut beantragt werden kann. Gerne können Sie diesbezüglich jedoch Kontakt mit den Administratoren des Dienstes aufnehmen: Senden Sie hierfür bitte eine Mail an <i>{{dao.admin_mail.replace('@', ' at ')}}</i>.
			% elif status['auto_allow'] or (short_url and status['is_activated']):
				Die URL wurde freigeschalten und ist unter der Kurz-URL <a href="{{short_url}}" id="shortURL" target="_blank">{{short_url}}</a> verfügbar.
				% if datestring and status['auto_allow']:
				  <br>Sie haben ein Datum angegeben - bei permanenten URLs ist ein Verfallsdatum nicht möglich. Die Kurz-URL wird daher dauerhaft im System bleiben und nicht
				  am {{datestring}} gelöscht.
				% end
			% elif status['is_under_consideration']:
				Die URL wurde bisher weder freigegeben noch abgelehnt. Bitte versuchen Sie es zu einem anderen Zeitpunkt nochmals. Sie werden dann an dieser Stelle die entsprechende Information dazu erhalten, wie der Status der Anfrage und gegebenenfalls die Kurz-URL lautet.
			% end
			</p>
		  % elif status['domain_allowed']:
			<p>Die Systembetreuer des URL-Kürzungsdienstes werden Ihre Anfrage prüfen und Ihnen an Ihre hinterlegte E-Mail-Adresse eine Antwort zukommen lassen. Dies geschieht i. d. R. innerhalb von 24 Stunden.</p>
		  % end
		  % if datestring:
			<p>Das angegebene Verfallsdatum wird nicht unberücksichtigt, da es sich um eine persistente URL handeln soll.</p>
		  % end
		% else:
		  % if not status['date_correct']:
			<p>Das angegebene Datumsformat ist nicht korrekt. Bitte geben Sie es in der Form <i>20.09.2016 16:15</i> oder <i>20.09.2016</i> ein. Das Datum darf nicht in der Vergangenheit liegen!</p>
		  % else:
			  <p>
				  Ihre Kurz-URL lautet: <a href="{{short_url}}" id="shortURL" target="_blank">{{short_url}}</a>
			  </p>

			% if datestring and not status['is_activated']:
			  <p>Sie wird automatisch am {{datestring}} Uhr gelöscht</p>
			% end
			% if status['is_activated']:
			  <p>Die URL existiert bereits in der Datenbank. Um Redundanz zu verhindern, wurde kein neuer Eintrag angelegt.
			  % if datestring:
			    Daher wird die Kurz-URL aber nicht zum eingegebenen Zeitpunkt gelöscht.
			  % end
			  </p>
			% end
		  % end
		% end
		% if url and short_url and (not purl_checkbox or (status['is_activated'] and not status['was_rejected'])):
  		  <p>Die eingegebene URL hat eine Länge von {{len(url)}} Zeichen; die gekürzte URL eine von {{len(short_url)}} Zeichen.</p>
		% end
	% end
	</div>
  % end
	<div>
		<input type="checkbox" name="showInfo" id="showInfo">
		<label for="showInfo">Nutzungshinweise ein-/ausblenden</label>
		<div class="hidden">
			<p>
				Für persistente URLs sind nur solche innerhalb der Domains der BAdW (inkl. LRZ und WMI) und des <a href="http://dhmuc.hypotheses.org/" target="_blank">dhmuc-Netzwerks</a> erlaubt.
				Nicht-persistente URLs hingegen können grundsätzlich auf jede Domain verweisen.
				Tragen Sie die zu kürzende URL bitte im vorgesehenen Feld ein.
				Sofern es eine permanente Kurz-URL werden soll, aktivieren Sie bitte die Checkbox.
			</p>
			<p>
				Hierfür ist die Eingabe einer gültigen E-Mail-Adresse nötig, damit wir Sie über den Verlauf Ihrer Anfrage informieren können.
				Bei nicht-persistenten Kurz-URLs kann ein Verfallsdatum angegeben werden. Bei Erreichen dieses Zeitpunktes wird die Kurz-URL automatisch deaktiviert.
				Die Funktionalität der Webseite ist auch ohne JavaScript gegeben.
			</p>
		</div>
	</div>
</article>
</main>
<script type="text/javascript">
	function check() {
		if (document.getElementById("url").value.replace(/\s+/g,"") === "") {
			return false;
		} else if (document.getElementById("purl_checkbox").checked && document.getElementById("purl_mail").value.replace(/\s+/g,"") === "") {
			alert("Bitte geben Sie eine E-Mail-Adresse an.\nAn diese senden wir Ihnen das Ergebnis der Überprüfung Ihrer Anfrage.\n\nVielen Dank!");
			return false;
		} else {
			return true;
		}
	}
</script>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)