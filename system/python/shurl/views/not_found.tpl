% error = kwargs.get('error', '')
<main>
	<article class="flush non-source sheet">
		<h3>Es trat ein Fehler bei diesem Seitenaufruf auf!</h3>
		% if error:
			{{error}}
		% end
	</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)