<main>
    <article class="flush non-source sheet autoWidth">
        <h2>Mein Bereich</h2>
        <p>
            Sie sehen hier die Aufrufe Ihrer eigenen URLs, sofern Sie deren Erhebung veranlasst haben.
        </p>
        % results = dao.get_my_area(kwargs['ldap_user'])
        % if results:
            % for db, entries in results.items():
                % if db == "shurl_tiny":
                    <h5><u>Kurz-URLs:</u></h5>
                % else:
                    <h5><u>Permalinks:</u></h5>
                % end

                % if entries:
                    <table>
                        <thead>
                        <tr>
                            <th style="width:5em;">Aufrufe</th>
                            <th style="width:6em !important;">Kurz-URL</th>
                            <th style="width:50% !important;">Ziel-URL</th>
                            <th style="width:10em;">Eintragsdatum</th>
                            <th style="width:10em;">{{'Ablaufdatum' if db == "shurl_tiny" else ''}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        % for entry in entries:
                            <tr>
                                <td>{{entry['cnt']}}</td>
                                <td>{{entry['short']}}</td>
                                <td class="breakWord"><a href="{{entry['url']}}" target="_blank" title="Öffnet diese URL in neuem Tab">{{entry['url']}}</a></td>
                                <td class="pl-0">{{entry['timestamp']}}</td>
                                <td class="pl-0">
                                    % if 'deletionDate' in entry:
                                        <span class="{{'abgelaufen' if entry['abgelaufen'] else 'gueltig'}}">
                                            {{entry['deletionDate'] or ""}}
                                        </span>
                                    % end
                                </td>
                            </tr>
                        % end
                        </tbody>
                    </table>
                % else:
                    Zu Ihrer Kennung sind aktuell noch keine {{"Kurz-URLs" if db == "shurl_tiny" else "Permalinks"}} vorhanden.
                % end
                <br>
            % end
        % else:
            Zu Ihrer Kennung sind aktuell noch keine Daten vorhanden.
        % end
    </article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)
