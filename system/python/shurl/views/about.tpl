<main>
	<article class="flush non-source sheet">
		<h2>Über Kurz-URLs</h2>
		<p>
			Eine sogenannte <i>Kurz-URL</i> ist eine Internetadresse<span class="note"><a class="note" data-sign="1" href="#n1"></a><small class="note" data-sign="1" id="n1">
				Das Akronym URL steht für <u>U</u>niform <u>R</u>esource <u>L</u>ocator und ist die Standardbezeichnung einer Netzwerkressource.
				Im Internet ist dies für gewöhnlich ein Server, der Daten im Web bereitstellt.
				Diese URLs können ca. 2000 Zeichen lang sein; reine Daten-URLs sogar über 10.000 Zeichen.<br/>
				Um längere URLs in wissenschaftlichen Publikationen oder auch in der privaten E-Mail leserlich darstellen zu können,
				gibt es Möglichkeiten, sie einer kürzeren und eindeutig zuordenbaren URL zuzuweisen.
			</small></span>, die aus wenigen Zeichen (i. d. R. Buchstaben und Zahlen) besteht. Für gewöhnlich sind auch die Domains, auf welchen Kurz-URLs angeboten werden, sehr kurz.
		</p>
		<p>
			Als berühmte und allgemein zugängliche Beispiele seien hierzu
			<a href="http://tinyurl.com" title="Kurz-URL-Dienst von TinyURL" target="_blank">TinyURL</a> und <a href="http://goo.gl" title="Kurz-URL-Dienst von Google" target="_blank">GooGl</a> genannt, die jedoch für akademische Zwecke leider unzureichend sind.
		</p>
		<p>
			Als Akademiker/in benötigt man einen Service zum Kürzen von URLs, der <u>schnell</u>, <u>unkompliziert</u> sowie <u>seriös</u> und <u>sicher</u> nutzbar ist.
			Letzteres bezieht sich natürlich auf die offensichtliche Werbung, allerdings auch auf die weniger sichtbare Speicherung von Nutzungsdaten.
		</p>
		<p>
			Der Kurz-URL-Service der BAdW garantiert Werbefreiheit sowie den vollständigen Verzicht auf die Erhebung von Nutzungsdaten - bei Zugriffen werden weder IP, Browser- und/oder Betriebssystemversion,
			Provider o.ä. Informationen abgespeichert.<br/>
			Lediglich der Zeitpunkt des Zugriffs auf unseren Service wird protokolliert, um ggbfs. die Konfiguration gemäß der Nutzungslast optimieren zu können.
		</p>
		<p>
			Im Folgenden finden Sie den Artikel <a href="/_/documents/Raaf-Über_Kurz_URLs.pdf" target="_blank"><i>Kurz-URLs in den Wissenschaften. Vor- und Nachteile</i></a>.
		</p>
	</article>
</main>\\
% rebase('base.tpl', request = request, dao = dao, kwargs = kwargs)