% status = kwargs.get('status')
% lang_id = kwargs.get('lang_id')
<main>
	<article class="flush non-source sheet autoWidth">
		<h3>Willkommen auf der Administrationsstartseite des Kurz-URL-Dienstes der BAdW</h3>
		<br>
		% if status:
			<div class="card">
			  % if status.get('action') == 'accept':
				<p>Die URL wurde freigeschalten.</p>
				% if status.get('user') and status.get('mail_text'):
				<p>Darüber wurde folgender Brief an <a href="mailto:{{status['user']}}">{{status['user']}}</a> gesandt:</p>
				<code>{{status['mail_text']}}</code>
				% end
			  % elif status.get('action') == 'refuse':
				% if not status['url_id']:
				<p>Der Aufruf erfolge leider ohne ID der PURL.</p>
				% else:
				<p>Die URL wurde nicht freigeschalten. Bitte informieren Sie den User darüber via E-Mail an <a href="mailto:{{status.get('user')}}">{{status.get('user')}}</a>.</p>
				% end
			  % elif status.get('action') == 'delete_refused':
				<p>Die abgelehnten PURLs wurden gelöscht.</p>
			  % end
			</div>
			<br>
		% end

		<h4><u>Folgende URLs sind eingetragen:</u></h4>
		% results = dao.get_entries_non_purl()
		% if results:
			<table>
				<thead>
				<tr>
					<th style="width: 45% !important;">URL</th>
					<th style="width: 10em !important;">Short-URL</th>
					<th>eingetragen am:</th>
				</tr>
				</thead>
				<tbody>
					% for url, url_id, timestamp in results:
						<tr>
							<td class="breakWord"><a href="{{url}}" target="_blank">{{url}}</a></td>
							<td><a href="{{request.urlparts[0]}}://{{request.urlparts[1].replace(dao.subdomain_for_purls, dao.subdomain_for_tiny)}}/{{url_id}}" target="_blank">{{url_id}}</a></td>
							<td>{{timestamp}}</td>
						</tr>
					% end
				</tbody>
			</table>
		% end

		<br>

		<h4><u>Folgende PURLs warten derzeit auf Freischaltung:</u></h4>
		% results = dao.get_entries_purl_under_consideration()
		% if results:
			<table>
				<thead>
				<tr>
					<th style="width: 45% !important;">URL</th>
					<th style="width: 10em !important;">Short-URL</th>
					<th>User:</th>
					<th>Aktion</th>
				</tr>
				</thead>
				<tbody>
					% for url, url_id, user in results:
						<tr>
							<td class="breakWord"><a href="{{url}}" target="_blank">{{url}}</a></td>
							<td><a href="{{request.urlparts[0]}}://{{dao.purl_domain}}/{{url_id}}" target="_blank">{{url_id}}</a></td>
							<td><a href="mailto:{{user}}">{{user}}</a></td>
							<td><a href="admin/accept?url_id={{url_id}}">freischalten</a>&nbsp;&nbsp;<a href="admin/refuse?url_id={{url_id}}">ablehnen</a></td>
						</tr>
					% end
				</tbody>
			</table>
		% end

		<br>

		<h4><u>Folgende PURLs sind bereits freigeschalten:</u></h4>
		% results = dao.get_entries_purl_activated()
		% if results:
			<table>
				<thead>
				<tr>
					<th style="width: 45% !important;">URL</th>
					<th style="width: 10em !important;">Short-URL</th>
					<th>User:</th>
					<th>Aktion</th>
				</tr>
				</thead>
				<tbody>
					% for url, url_id, user in results:
						<tr>
							<td class="breakWord"><a href="{{url}}" target="_blank">{{url}}</a></td>
							<td><a href="{{request.urlparts[0]}}://{{dao.purl_domain}}/{{url_id}}" target="_blank">{{url_id}}</a></td>
							<td><a href="mailto:{{user}}">{{user}}</a></td>
							<td><a href="admin/refuse?url_id={{url_id}}">ablehnen</a></td>
						</tr>
					% end
				</tbody>
			</table>
		% end

		<br>

		<h4><u>Folgende PURLs wurden abgelehnt:</u></h4>
		% results = dao.get_entries_purl_refused()
		% if results:
			<table>
				<thead>
				<tr>
					<th style="width: 45% !important;">URL</th>
					<th style="width: 10em !important;">Short-URL</th>
					<th>User:</th>
					<th>Aktion</th>
				</tr>
				</thead>
				<tbody>
					% for url, url_id, user in results:
						<tr>
							<td class="breakWord"><a href="{{url}}" target="_blank">{{url}}</a></td>
							<td><a href="{{request.urlparts[0]}}://{{dao.purl_domain}}/{{url_id}}" target="_blank">{{url_id}}</a></td>
							<td><a href="mailto:{{user}}">{{user}}</a></td>
							<td><a href="admin/accept?url_id={{url_id}}">freischalten</a></td>
						</tr>
					% end
				</tbody>
			</table>
			<br>
			<a style="color:red;" href="/{{lang_id}}/admin/delete_refused">alle abgelehnten PURLs löschen</a>
		% end
	</article>
</main>\\
% rebase('admin_base.tpl', request = request, dao = dao, kwargs = kwargs)
