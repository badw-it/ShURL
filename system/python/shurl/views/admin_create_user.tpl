<main>
	<article class="flush non-source sheet">
		<h3>Nutzer hinzufügen</h3>
	  % if request.method == 'POST':
		<div class="card">
			<p class="systemMessage">Systemmitteilung:</p>
		  % if not kwargs.get('valid_name'):
			<p>Der Nutzername darf weder leer sein noch ausschließlich aus Leerzeichen bestehen!</p>
		  % elif not kwargs.get('valid_password'):
			<p>Das Passwort darf weder leer sein noch ausschließlich aus Leerzeichen bestehen!</p>
		  % elif kwargs.get('name_exists'):
			<p>Es existiert bereits ein Nutzer namens <i>{{request.forms.userName}}</i>.</p>
		  % elif kwargs.get('error'):
			<p>Es ist ein Fehler aufgetreten:</p>
			<code>{{kwargs['error']}}</code>
		  % else:
			<p>Der Nutzer <i>{{request.forms.userName}}</i> wurde erfolgreich angelegt.</p>
		  % end
		</div>
	  % end
		<form method="post" onSubmit="return check();">
			Bitte legen Sie hier einen neuen Nutzer für den Administrations-Bereich an.<br>
			Der Nutzer sollte sein Passwort nach dem ersten Login ändern!<br><br>
			<div class="table">
				<div>
					<div>Login-Name:</div>
					<div><input type="text" class="normal" name="userName" id="userName" value=""></div>
				</div>
				<div>
					<div>Passwort:</div>
					<div><input type="password" class="normal" name="userPassword" id="userPassword" value=""></div>
				</div>
			</div>
			<input type="submit" name="submit" value="Nutzer anlegen">
		</form>
	</article>
</main>

<script type="text/javascript">
	function check() {
		if (document.getElementById("userPassword").value.replace(/\s+/g,"") === "") {
			alert("Das angegebene Passwort darf nicht leer sein oder nur aus Leerzeichen bestehen!");
			return false;
		} else {
			return true;
		}
	}
</script>\\
% rebase('admin_base.tpl', request = request, dao = dao, kwargs = kwargs)
