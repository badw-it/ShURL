# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Manuel Raaf in 2016 ff. © http://badw.de

import hashlib
import random
from contextlib import closing
from hashlib import sha512
from ldap3 import Server, Connection, ALL_ATTRIBUTES, SUBTREE

def create_salt():
    return str(random.getrandbits(1024)).encode("utf-8")

def get_hash_and_salt(password):
    salt = create_salt()
    return (sha512(salt + password.encode("utf-8")).hexdigest(), salt)

def admin_login(user, password, session, connect, db_name):        
    if 'admin' in session:
        return True
    if not all((user, password, session)):
        return False
   
    with closing(connect(db=db_name)) as connection:
        cursor = connection.cursor()
        sql = "SELECT salt, password FROM user WHERE user COLLATE utf8_bin = %s"
        cursor.execute(sql, (user,))
        result = cursor.fetchone()
        (salt, stored_password) = (
                (result['salt'], result['password']) if result else
                (None, None))
        if salt:
            hashed_password = hashlib.sha512(
                    salt.encode("utf-8") + password.encode("utf-8")
                    ).hexdigest()
            if hashed_password == stored_password:
                session['admin'] = user
                return True            
    try:
        session['admin'] = None
        session.invalidate()
        session.delete()
    except:
        pass
    return False

def admin_logout(session):
    try:
        session['admin'] = None
        session.invalidate()
        session.delete()
    except:
        pass

def login(user, password, session, connect, db_name, ldap = None):           
    if 'user' in session:
        return True
    if not all((user, password, session)):
        return False    
    if not ldap:   
        with closing(connect(db=db_name)) as connection:
            cursor = connection.cursor()
            sql = "SELECT salt, password FROM user WHERE user COLLATE utf8_bin = %s"
            cursor.execute(sql, (user,))
            result = cursor.fetchone()
            (salt, stored_password) = (
                    (result['salt'], result['password']) if result else
                    (None, None))
            if salt:
                hashed_password = hashlib.sha512(
                        salt.encode("utf-8") + password.encode("utf-8")
                        ).hexdigest()
                if hashed_password == stored_password:
                    session['user'] = user
                    return True                
        try:
            session['user'] = None
            session.invalidate()
            session.delete()
        except:
            pass
        return False
    else:
        return ldap_login(user, password, ldap, session)

def logout(session):
    try:
        session['user'] = None
        session.invalidate()
        session.delete()
    except:
        pass

def ldap_login(user='', password='', connection_info=None, session=None, token=None, debug=None):
    if user.strip() == '' or password.strip() == '' or not connection_info:        
        return (False, '',)   

    msg = ''
    success = False
    try:
        with Connection(Server(connection_info['server'], use_ssl=connection_info['use_ssl'],
                           get_info=None),
                           user=connection_info['adminUser'],
                           password=connection_info['adminPassword'],
                           auto_bind=False,
                           read_only=True,
                           lazy=False,
                           raise_exceptions=False,
                           ) as c:
            if c.search(search_base=connection_info['intranet'],
                    search_filter=connection_info['search_filter'] % user,
                    search_scope=SUBTREE,
                    attributes=ALL_ATTRIBUTES,
                    get_operational_attributes=True):

                if debug:
                    print(c.response_to_json())
            
                comparison = c.compare(connection_info['user'] % user, 'userPassword' , password)
                if comparison:
                    success = True
                else:
                    msg += "user and/or password could not be found!"
            else:
                msg += "user could not be found!"
    except Exception as e:
        print(e)
        pass

    if success and session is not None:
        if (token and session['token'] == token) or not token:
            session['user'] = user
        else:
            msg += "wrong token"
            logout(session)
            success = False
    if not success and msg:
        print(msg)
    return success
