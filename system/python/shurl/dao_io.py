import json
import os
import re
import smtplib, ssl
import datetime
import random
import traceback
from contextlib import closing
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from functools import partial

import pymysql as mysql

import __init__
from shurl import auth
from shurl import shortening

s = re.search
sub = re.sub

DB_NAMES = ('shurl_tiny', 'shurl_purl')
# The order **must** be: first the tiny, then the purl database.

class DAO():
    '''
    Class of a data access object.
    '''
    def __init__(self, config_path: str, badwords_path: str) -> None:
        config_path = os.path.abspath(config_path)
        with open(config_path, 'r', encoding = 'utf-8') as file:
            config = json.load(file)
        for key, value in config.items():
            setattr(self, key, value)

        badwords = ()
        if badwords_path:
            try:
                with open(os.path.abspath(badwords_path), 'r', encoding = 'utf-8') as file:
                    badwords = file.read().splitlines()
                badwords.append("strauss")
                regex = re.compile(r'\W')
                i=0
                for badword in badwords:    
                    if re.search(regex, badword):
                        badwords.pop(i)
                    i += 1
            except:
                badwords = ()
                pass

        self.badwords = badwords
        self.external_port = None
        self.mail_text = "".join(self.mail_text)
        self.path = os.path.abspath(
                os.path.join(
                    os.path.dirname(config_path), self.public_path)) + os.sep
        self.session_options['session.data_dir'] = os.path.abspath(
                os.path.join(
                    os.path.dirname(config_path), self.session_options['session.data_dir']))
        self.purl_domain = '.'.join(
                (self.subdomain_for_purls, self.domain)).strip().strip('.')

        self.connect = partial(mysql.connect,
                host        = self.host,
                port        = self.port,
                passwd      = self.password,
                user        = self.user,
                charset     = self.charset,
                cursorclass = mysql.cursors.DictCursor,
                autocommit  = True,
                )
        # Note: :param:`db` is added while calling this partial in order
        # to distinguish between purl and non-purl case.

    def empty_status(self, url_id='', user=''):
        return {
                'url_id': url_id,
                'mail_text' : '',
                'user' : user,
                'domain_allowed': True,
                'date_correct': True,
                'date': None,
                'page_available': True,
                'was_rejected': False,
                'is_activated': False,
                'is_under_consideration': False,
                'error': '',
                'auto_allow': False,
            }
    
    def accept(self, url_id, host = '', auto_allow=False):
        status = self.empty_status()
        if url_id:
            with closing(self.connect(db = DB_NAMES[True])) as connection:
                cursor = connection.cursor()
                cursor.execute('UPDATE shurl SET active=1 WHERE short COLLATE utf8_bin = %s', url_id)
                connection.commit()
                cursor.execute('SELECT short, url, user FROM shurl WHERE short COLLATE utf8_bin = %s AND isDeleted = 0', url_id)
                try:
                    result = cursor.fetchone()
                    url_id, url, user = result['short'], result['url'], result['user']
                    status['user'] = user
                    short_url = self.get_short_purl(url_id)
                    if short_url[:5].lower() == 'http:':
                        short_url = "https:%s" % short_url[5::]
                    mail_text = self.mail_text_accept.format(
                            url = url, short_url = short_url)
                    if not auto_allow:
                        self.send_mail(self.admin_mail, user, '', host,
                            self.mail_text_accept_subject, mail_text)
                    status['mail_text'] = mail_text
                except Exception as e:
                    print(e)
                    pass
        return status

    def refuse(self, url_id):
        status = {'action': 'refuse', 'url_id': url_id, 'user': ''}
        if url_id:
            with closing(self.connect(db = DB_NAMES[True])) as connection:
                cursor = connection.cursor()
                cursor.execute('UPDATE shurl SET active=0 WHERE short COLLATE utf8_bin = %s', url_id)
                connection.commit()
                cursor.execute('SELECT user FROM shurl WHERE short COLLATE utf8_bin = %s AND isDeleted = 0', url_id)
                user = cursor.fetchone().get('user')
                status['user'] = user
        return status

    def delete_refused(self):
        status = {'action': 'delete_refused'}
        with closing(self.connect(db = DB_NAMES[True])) as connection:
            cursor = connection.cursor()
            cursor.execute('DELETE FROM shurl WHERE active=0')
            connection.commit()
        return status

    def change_password(self, password, user):
        hashed_password, salt = auth.get_hash_and_salt(password)
        sql = "UPDATE user SET salt=%s, password=%s WHERE user COLLATE utf8_bin = %s"
        with closing(self.connect(db = self.db_for_credentials)) as connection:
            cursor = connection.cursor()
            cursor.execute(sql, (salt, hashed_password, user))
            connection.commit()

    def check_purl_domains(self, url) -> bool:        
        domain_match = s(r'//([^/]+)', url)
        domain_parts = domain_match.group(1).split(':')[0].split('.')
        domain = '.'.join(domain_parts[-2:])
        if domain in self.allowed_domains_for_purls:
            return True
        elif '.'.join(domain_parts) in self.allowed_domains_for_purls:
            return True 
        return False

    def purl_auto_allow(self, url) -> bool:
        domain_match = s(r'//([^/]+)', url)
        if domain_match.group(1) in self.auto_allow_purls:
            return True
        return False
    
    def create_user(self, user, password):
        hashed_password, salt = auth.get_hash_and_salt(password)
        with closing(self.connect(db = self.db_for_credentials)) as connection:
            cursor = connection.cursor()
            sql = "SELECT id FROM user WHERE user COLLATE utf8_bin = %s"
            cursor.execute(sql, user)
            if (cursor.fetchone() or {}).get('id'):
                return True
            sql = "INSERT INTO user (user, salt, password) VALUES (%s, %s, %s)"            
            cursor.execute(sql, (user, salt, hashed_password))
            connection.commit()
        return False

    def ensure_url_protocol(self, url) -> str:
        if not s(r'^[^:/.]+://', url):
            url = 'http://' + url
        return url

    def get_entries_non_purl(self):
        is_purl = False
        with closing(self.connect(db = DB_NAMES[is_purl])) as connection:
            cursor = connection.cursor()
            cursor.execute('SELECT url, short, timestamp FROM shurl WHERE isDeleted = 0 ORDER BY timestamp DESC')
            return [ (item['url'], item['short'], item['timestamp']) for item in cursor.fetchall() ]

    def get_entries_purl_activated(self):
        is_purl = True
        with closing(self.connect(db = DB_NAMES[is_purl])) as connection:
            cursor = connection.cursor()
            cursor.execute('SELECT url, short, user FROM shurl WHERE active=1 AND isDeleted = 0 ORDER BY timestamp DESC')
            return [ (item['url'], item['short'], item['user']) for item in cursor.fetchall() ]

    def get_entries_purl_refused(self):
        is_purl = True
        with closing(self.connect(db = DB_NAMES[is_purl])) as connection:
            cursor = connection.cursor()
            cursor.execute('SELECT url, short, user FROM shurl WHERE active=0 AND isDeleted = 0 ORDER BY timestamp DESC')
            return [ (item['url'], item['short'], item['user']) for item in cursor.fetchall() ]

    def get_entries_purl_under_consideration(self):
        is_purl = True
        with closing(self.connect(db = DB_NAMES[is_purl])) as connection:
            cursor = connection.cursor()
            cursor.execute('SELECT url, short, user FROM shurl WHERE active IS NULL AND isDeleted = 0 ORDER BY timestamp DESC')
            return [ (item['url'], item['short'], item['user']) for item in cursor.fetchall() ]

    def get_short_purl(self, url_id):
        return self.ensure_url_protocol(
                '{}/{}'.format(self.purl_domain, url_id))

    def get_url(self, url_id, is_purl, referer) -> str:
        with closing(self.connect(db = DB_NAMES[is_purl])) as connection:
            cursor = connection.cursor()
            if self.log_accesses:
                sql = "INSERT INTO log (short, readWrite, referrer) VALUES (%s, %s, %s)"
                cursor.execute(sql, (url_id, 'r', referer))
                connection.commit()
            sql = "SELECT url FROM shurl WHERE short COLLATE utf8_bin = %s AND isDeleted = 0"
            if is_purl:
                sql += ' AND active=1'
            cursor.execute(sql, url_id)            
            return (cursor.fetchone() or {'url':''})['url']

    def get_url_id(self, url, is_purl, date, purl_mail, ldap_user):
        url_id = ''
        status = self.empty_status()
        if not s(r'^(http|ftp)s?://', url):
            url = 'http://' + url
        if is_purl and not self.check_purl_domains(url):
            status['domain_allowed'] = False
            # Wait with return after check whether the url was already rejected.
        date = date if date else None
        if date and not is_purl:
            date = format_date(date)            
            if not date:
                status['date_correct'] = False
                return url_id, status
            else:
                status['date'] = date
                # May be overwritten below: If there already is an entry for the URL. See #### TODO
        if not shortening.is_url_available(url):
            status['page_available'] = False
            return url_id, status

        with closing(self.connect(db = DB_NAMES[is_purl])) as connection:
            cursor = connection.cursor()
            try:
                if is_purl:
                    sql = "SELECT id, short, active FROM shurl WHERE url COLLATE utf8_bin = %s AND short IS NOT NULL AND isDeleted = 0"
                    cursor.execute(sql, (url,))
                    result = cursor.fetchone() or None
                    if result and result['short']:
                        if result['active'] == 0:
                            status['was_rejected'] = True
                            url_id = ''
                        elif result['active'] == 1:
                            status['is_activated'] = True
                            url_id = result['short']
                        else:
                            status['is_under_consideration'] = True
                            url_id = ''
                        return url_id, status
                    if not status['domain_allowed'] and not self.purl_auto_allow(url):
                        return '', status
                else:
                    sql = "SELECT id, short FROM shurl WHERE url COLLATE utf8_bin = %s AND short IS NOT NULL AND isDeleted = 0"
                    cursor.execute(sql, (url,))
                    result = cursor.fetchone() or None
                    if result and result['short']:
                        status['is_activated'] = True
                        #status['date'] = None #### TODO: fetch it from db and tell it in template?
                        return result['short'], status
                    
                if is_purl:
                    if self.id_is_autoincrement is False:
                        sql = "INSERT INTO shurl (id, url, user, ldap_user) VALUES (%s, %s, %s, %s)"
                    else: sql = "INSERT INTO shurl (url, user, ldap_user) VALUES (%s, %s, %s)"
                else:
                    if self.id_is_autoincrement is False:
                        sql = "INSERT INTO shurl (id, url, deletionDate, ldap_user) VALUES (%s, %s, %s, %s)"
                    else: sql = "INSERT INTO shurl (url, deletionDate, ldap_user) VALUES (%s, %s, %s)"

                last_id = None
                while not last_id:
                    #print(sql, url, purl_mail if is_purl else date)
                    if self.id_is_autoincrement:
                        cursor.execute(sql, (url, purl_mail if is_purl else date, ldap_user))
                        last_id = cursor.lastrowid                        
                    else:
                        last_id = self.get_random_id(cursor)
                        cursor.execute(sql, (last_id, url, purl_mail if is_purl else date, ldap_user))
                    
                    url_id = shortening.encode(last_id)
                    if url_id.lower() in self.badwords:
                        cursor.execute("DELETE FROM shurl WHERE id=%s", (last_id,))
                        last_id = None

                cursor.execute("UPDATE shurl SET short=%s WHERE id=%s", (url_id, last_id))
                if is_purl and self.purl_auto_allow(url):
                    status = self.accept(url_id, '', True)
                    status['is_activated'] = True
                    status['auto_allow'] = True
                connection.commit()
            except Exception as e:
                status['error'] = str(e)                
            return url_id, status

    def get_random_id(self, cursor):
        random.seed()
        random_id = None
        while not random_id:
            random_id = random.randint(self.random_id_minimum, self.random_id_maximum)
            cursor.execute("SELECT 1 FROM shurl WHERE id = %s", (random_id,))
            try:
                if cursor.rowcount > 0:
                    random_id = None
            except Exception as e:
                random_id = None        
        return random_id

    def log(self, is_url, url_id, url, read_or_write, referrer):
        with closing(self.connect(db = DB_NAMES[is_url])) as connection:
            cursor = connection.cursor()
            try:
                sql="INSERT INTO log (short,url,readWrite,referrer) VALUES (%s, %s, %s, %s)"
                cursor.execute(sql, (url_id, url, read_or_write, referrer))
                connection.commit()
            except:
                pass

    def send_mail(self, fromUser=None, toUser=None, url=None, host=None, subject=None, text=None):
        try:
            content = MIMEMultipart('alternative')
            content['Subject'] = self.mail_subject if not subject else subject
            # hier steckt noreply@badw.de drin aufgrund der Config
            content['From'] = self.mail_smtp_auth['mail']
            content['To'] = self.admin_mail if not toUser else toUser
            content['Reply-To'] = self.admin_mail if not fromUser else fromUser

            if not text:
                text = self.mail_text.format(content['From'], content['From'], url, url, host)

            plain = MIMEText(strip_tags(text), 'plain')
            html = MIMEText(text, 'html')

            content.attach(plain)
            content.attach(html)

            host, port = self.mail_smtp_auth['host'], self.mail_smtp_auth['port']
            with smtplib.SMTP(host=host, port=port) as server:
                if self.mail_smtp_auth['_use_auth'] == 1:
                    context = ssl.create_default_context()
                    server.starttls(context=context)
                    server.login(self.mail_smtp_auth['user'], self.mail_smtp_auth['password'])
                server.send_message(content)
                return "Die E-Mail an <i>" + content['To'] + "</i> mit dem Betreff <i>" + content['Subject'] + "</i> wurde erfolgreich gesendet"
        except Exception as e:
            traceback.form_exec(e)
            return e

    def get_my_area(self, ldap_user):
        result = {}
        sql = """
            SELECT
                shurl.*, COUNT(*) as cnt
                -- , IF(shurl.deletionDate IS NOT NULL, UNIX_TIMESTAMP(shurl.deletionDate) <= UNIX_TIMESTAMP(NOW()), 0) as abgelaufen
            FROM log LEFT JOIN shurl ON shurl.short = log.short
            WHERE shurl.ldap_user = %s AND log.readWrite = 'r'
            GROUP BY shurl.short
            ORDER BY cnt DESC
            """
        for db in ["shurl_tiny", "shurl_purl"]:
            result[db] = {}
            with closing(self.connect(db = db)) as connection:
                cursor = connection.cursor()
                try:
                    cursor.execute(sql.replace("-- ", "") if db == "shurl_tiny" else sql, (ldap_user))
                    try:
                        result[db] = cursor.fetchall()
                    except Exception as e:
                        print(e)
                        pass
                except:
                    pass
#         print(result)
        return result
def format_date(date):
    zero = re.compile(r'^0')
    NaN = re.compile(r'\D')
    year, month, day, hour, minute = "", "", "", "", ""    
    cnt = 1
    for match in re.finditer(r'\d+', date):
        if cnt == 1:
            day = match.group()
            if re.match(NaN, day):
                break
            elif int(day) < 10 and not re.match(zero, day):
                day = "0" + day
        elif cnt == 2:
            month = match.group()
            if re.match(NaN, month):
                break
            elif int(month) < 10 and not re.match(zero, month):
                month = "0" + month
        elif cnt == 3:
            year = match.group()
            if re.match(NaN, year):
                break
            elif re.match(r'^\d{2}$', year):
                year = "20" + year            
        elif cnt == 4:
            hour = match.group()
            if re.match(NaN, hour):
                break
            elif int(hour) < 10 and not re.match(zero, hour) :
                hour = "0" + hour
        elif cnt == 5:
            minute = match.group()
            if re.match(NaN, minute):
                break
            elif int(minute) < 10 and not re.match(zero, minute):
                minute = "0" + minute
        cnt += 1

    if not minute:
        minute = "00"
    if not hour:
        hour = "00"    
    if not year:
        year = datetime.datetime.now().year

    try:
        date = datetime.datetime(int(year), int(month), int(day), int(hour), int(minute))
        #.strftime('%Y-%m-%d %H:%M:%S')
        if date < datetime.datetime.now():
            date = False            
    except:
        date = False

    return date

def strip_tags(string):
    return sub(r'<[^>]*>', "", sub(r'<br>', "\n", string))
