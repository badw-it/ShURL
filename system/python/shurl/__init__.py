# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff, Manuel Raaf in 2019 ff. © http://badw.de
import os, sys
import inspect

path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
if sys.path[0] != path:
    sys.path[0] = path

def val2bool(value='', evaluate_to_false={'', 'false', 'null', 'none', 'undef'}):    
    try:
        value = int(value)
        if value < 0:
            value = 0
    except:
        pass    
    return (False if value.lower() in evaluate_to_false else True) if isinstance(value, str) else bool(value)

frame=inspect.currentframe()
frame=frame.f_back.f_back
code=frame.f_code
print(code, frame)

port = None
try:
    port = int(sys.argv[1])    
    if port < 1:
        port = None
except:
    port = None
    pass

debug = False
try:
    debug = val2bool(sys.argv[2])
except:
    debug = False
    pass

print("starting on port", port, "with debug-mode set to:", debug)
