function ready(fn) {
    if (document.readyState !== 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}
ready(function() {
    if (document.getElementById("shortURL")) {
        var copyButton = document.createElement('span');
        copyButton.setAttribute("id", "copyButton");
        copyButton.innerHTML = '<span onClick="copyURL();" title="Kopiere die URL in die Zwischenablage">⎘</span>';
        var generateButton = document.getElementById("submitButton");
        generateButton.parentNode.insertBefore(copyButton, generateButton.nextSibling);
    }
    window.setTimeout(
        function() {
            document.body.classList.remove("d-none");
        }, 1500
    );
});

function copyURL() {
    try {
        var copyText = document.getElementById("shortURL");
        if (copyText) {
            if (!navigator.clipboard) {
                var dummy = document.createElement('textarea');
                dummy.value = copyText.innerHTML;
                dummy.setAttribute("style", "left:-9999999px !important;");
                document.body.appendChild(dummy);
                dummy.select();
                dummy.setSelectionRange(0, 99999); /* For mobile devices */
                document.execCommand('copy');
            } else {
                navigator.clipboard.writeText(copyText.innerHTML);
            }
            try {
                document.body.removeChild(dummy);
            } catch (e) {
                //pass
            }
            var copyButton = document.getElementById('copyButton');
            var text = document.createElement('span');
            text.setAttribute('id', 'copyInfo');
            text.innerHTML = 'Die Kurz-URL wurde in die Zwischenablage kopiert.';
            copyButton.parentNode.insertBefore(text, copyButton.nextSibling);
            window.setTimeout(function () {
                document.getElementById('copyInfo').parentNode.removeChild(text);
            }, 1750);
        }
    } catch (e) {
        console.log(e);
    }
}