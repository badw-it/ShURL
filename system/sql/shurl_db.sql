-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 15. Feb 2024 um 10:03
-- Server-Version: 10.6.15-MariaDB-log
-- PHP-Version: 8.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Datenbank: `shurl_purl`
--
DROP DATABASE IF EXISTS `shurl_purl`;
CREATE DATABASE IF NOT EXISTS `shurl_purl` DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci;
USE `shurl_purl`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
    `id` bigint(20) UNSIGNED NOT NULL,
    `short` varchar(100) NOT NULL,
    `url` text DEFAULT NULL,
    `readWrite` char(1) NOT NULL,
    `referrer` varchar(255) DEFAULT NULL,
    `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shurl`
--

DROP TABLE IF EXISTS `shurl`;
CREATE TABLE `shurl` (
    `id` bigint(20) UNSIGNED NOT NULL,
    `url` text NOT NULL,
    `short` varchar(100) DEFAULT NULL,
    `ldap_user` varchar(10) DEFAULT NULL,
    `user` varchar(1000) DEFAULT NULL,
    `active` tinyint(1) UNSIGNED DEFAULT NULL,
    `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
    `checksum` char(44) DEFAULT NULL,
    `isDeleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `log`
--
ALTER TABLE `log`
    ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `shurl`
--
ALTER TABLE `shurl`
    ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`(255)),
  ADD KEY `ldap_user` (`ldap_user`),
  ADD KEY `isDeleted` (`isDeleted`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `log`
--
ALTER TABLE `log`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Datenbank: `shurl_tiny`
--
DROP DATABASE IF EXISTS `shurl_tiny`;
CREATE DATABASE IF NOT EXISTS `shurl_tiny` DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci;
USE `shurl_tiny`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
    `id` bigint(20) UNSIGNED NOT NULL,
    `short` varchar(100) NOT NULL,
    `url` text DEFAULT NULL,
    `readWrite` char(1) NOT NULL,
    `referrer` varchar(255) DEFAULT NULL,
    `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shurl`
--

DROP TABLE IF EXISTS `shurl`;
CREATE TABLE `shurl` (
    `id` bigint(20) UNSIGNED NOT NULL,
    `url` text NOT NULL,
    `short` varchar(100) DEFAULT NULL,
    `ldap_user` varchar(10) DEFAULT NULL,
    `deletionDate` timestamp NULL DEFAULT NULL,
    `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
    `checksum` char(44) DEFAULT NULL,
    `isDeleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
                        `id` int(10) UNSIGNED NOT NULL,
                        `user` varchar(100) NOT NULL,
                        `password` char(128) NOT NULL,
                        `salt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `log`
--
ALTER TABLE `log`
    ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `shurl`
--
ALTER TABLE `shurl`
    ADD PRIMARY KEY (`id`),
  ADD KEY `ldap_user` (`ldap_user`),
  ADD KEY `isDeleted` (`isDeleted`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
    ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `log`
--
ALTER TABLE `log`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;
